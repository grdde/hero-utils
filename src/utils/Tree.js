/****
 前端树结构实现，根据实际情况进行修改
 ***/


const levenArr = ["hostId", "processId", "participantId", "parentId", "dataId"];

class GenderTree {
    constructor(json) {
        this.tree = {}
        this.levenIndex = 0
        this.json = json || []
    }

    genderFiled(data, filed, nameFiled) {
        return {
            key: data[filed],
            hostId: data.hostId,
            entity: data[nameFiled],
            kind: data.kind,
            nodeType: data.nodeType,
            descriptionName: data.descriptionName,
            topicType: data.topicType,
            topicName: data.topicName,
        }
    }
    gender(){
        const leven = levenArr[this.levenIndex];
        for (let i = 0; i < this.json.length; i++) {
            let item = this.json[i];
            let hostObj = this.tree[item.hostName];
            let processObj = hostObj?.[item.processId]
            let participantObj = processObj?.[item.participantId]
            let parentObj = participantObj?.[item.parentId]
            let dataObj = parentObj?.[item.dataId]
            switch (leven) {
                case "hostId":
                    if (!hostObj) {
                        hostObj = this.tree[item.hostName] = {
                            data: this.genderFiled(item, "hostId", "hostName")
                        }
                    }
                    break;
                case "processId":
                    if (!processObj) {
                        let data = { description:'id=' + item.processId }
                        Object.assign(data,this.genderFiled(item, "processId", "processName"))
                        processObj = hostObj[item.processId] = { data }
                    }
                    break;
                case "participantId":
                    if (!participantObj) {
                        participantObj = processObj[item.participantId] = {
                            data: this.genderFiled(item, "participantId", "participantName")
                        }
                    }
                    break;
                case "parentId":
                    if (!parentObj) {
                        parentObj = participantObj[item.parentId] = {
                            data: this.genderFiled(item, "parentId", "parentName")
                        }
                    }
                    break;
                case "dataId":
                    if (!dataObj) {
                        dataObj = parentObj[item.dataId] = []
                    }
                    dataObj.push(Object.assign(this.genderFiled(item, "dataId", "dataName"), {
                        topicName: item.topicName,
                        topicType: item.topicType,
                        kkk: item.nodeType,
                        treeType: item.topicType,
                        description: item.topicName,
                        toType: item.nodeType,
                    }))
                    break;
            }
        }
        // console.log(this.json,'tttttttttt')
        if (this.levenIndex++ < levenArr.length) {
            this.gender();
        }
    }

    tree2list(tree){
        let res = []
        for (let key in tree) {
            let node = tree[key]
            if(node?.data){
                let cloneNode = Object.assign({},node)
                delete cloneNode.data
                let children = this.tree2list(cloneNode)
                if(Array.isArray(children[0])){
                    children = [...children[0]]
                }
                res.push({
                    ...node.data,
                    children
                })
                continue
            }
            res.push(node)
        }
        return res
    }

    genderAnd2list(){
        this.gender();
        return this.tree2list(this.tree)
    }
}
export default GenderTree
