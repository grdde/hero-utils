# hero-utils
##介绍
```
记录学习工作中遇见的一些方法
一起学习、一起进步。
```
##使用
```
utils文件下添加新的方法
测试页随意
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
